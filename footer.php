<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
?>
	<?php do_action('wp_content_bottom'); ?>
	</div>


	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>
		
	<footer id="site-footer">
		<div>
			<div>
				<div id="site-footer-main">
					<div class="container m-auto py-4 px-md-3 row justify-content-between align-items-start">
					<?php
     $footer_address = get_field("footer_address", "options");
     $address_card_blurb = get_field("footer_blurb", "options");
     $mcc_app = get_field("mcc_app", "options");
     ?>
					<div class="footer-address-card mb-3 mb-md-0 col-md-6 col-lg-3">
						<img class="mb-2" src="<?php echo do_shortcode("[lg-logo]"); ?>" alt="">
						<div class="mb-3"><?php echo $address_card_blurb; ?></div>
				        <div class="mb-3"><?php echo do_shortcode("[lg-social-media]"); ?></div>
				        <div class="mcc-app">
					        <p class="mb-2">MCC Home Centre App</p>
							<?php if (have_rows('mcc_app', "options")): ?>
								<?php while (have_rows('mcc_app', "options")):
        	the_row(); ?>
									<?php
         $mcc_app_image = get_sub_field('mcc_app_image', 'options');
         $mcc_app_link = get_sub_field('mcc_app_link', 'options');
         ?>
									 <a href="<?php echo $mcc_app_link['url']; ?>" target="<?php echo $mcc_app_link[
	'target'
]; ?>"><img src="<?php echo $mcc_app_image['url']; ?>" alt="<?php echo $mcc_app_image[
	'alt'
]; ?>"> </a>
								<?php
        endwhile; ?>
							<?php endif; ?>
				        </div>	

					</div>
					<div class="footer-get-in-touch mb-3 mb-md-0 col-md-6 col-lg-3">
						<h2 class="text-white" class="h5">Get in Touch</h2>
						<?php $get_in_touch = get_field("get_in_touch", "option"); ?>
						<?php echo $get_in_touch; ?>
					</div>
					<div class="quick-links mb-3 mb-md-0 col-md-6 col-lg-3">
						<h2 class="h5">Quick Links</h2>
						<div class="footer-quick-links">
						<?php
      $footerQuickLinks = array(
      	'menu' => 'quick-links',
      	'depth' => 2
      );
      wp_nav_menu($footerQuickLinks);
      ?>
				        </div>
				        <div class="footer-quick-links-2">
						<?php
      $footerQuickLinks2 = array(
      	'menu' => 'quick-links-2',
      	'depth' => 2
      );
      wp_nav_menu($footerQuickLinks2);
      ?>
				        </div>
					</div>
					<div class="footer-facebook-feed col-md-6 col-lg-3">
						<div class="footer-instagram-ajax">
							<?php echo do_shortcode("[custom-facebook-feed]"); ?>
						</div>
					</div>
				</div>
				</div>
			</div>
			<?php if (!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
				<div id="site-legal" class="py-3 px-3">
					<div class="d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
						<div class="site-info"><?php get_template_part(
      	"/templates/template-parts/footer/site-info"
      ); ?></div>
						<div class="site-longevity"> <?php get_template_part(
      	"/templates/template-parts/footer/site-footer-longevity"
      ); ?> </div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action('document_end'); ?>
