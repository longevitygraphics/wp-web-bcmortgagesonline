<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<?php
		$banner = get_option('lg_option_blog_archive_banner_image');
		$banner_height = get_option('lg_option_blog_archive_banner_height') ? get_option('lg_option_blog_archive_banner_height') : '400px';
		$blog_style = get_option('lg_option_blog_style') ? get_option('lg_option_blog_style') : 'list';
	?>

	<main class="blog">
		<?php if($banner): ?>
			<div class="blog-banner top-banner">
				<div class="blog-page-banner-image">
					<img src="<?php echo $banner; ?>">
				</div>
				<div class="top-banner-overlay container">
					<h1>Blog</h1>
				</div>
			</div>
		<?php endif; ?>

		<div class="container">
			<?php
				switch ($blog_style) {
				    case "list":
				        get_template_part( 'templates/template-parts/blog/list');
				        break;
				    case "grid":
				        get_template_part( 'templates/template-parts/blog/grid');
				        break;
				    case "masonary":
				        get_template_part( 'templates/template-parts/blog/masonary');
				        break;
				}
			?>
		</div>
	</main>

<?php get_footer(); ?>