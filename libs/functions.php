<?php
function main_nav_items ( $items, $args ) {
	//lg_write_log($args);
    if ($args->menu == 'main-nav') {
        $items .= '<li class="d-none d-lg-inline-block nav-phone menu-item menu-item-type-custom menu-item-object-custom nav-item ml-3"><a class="nav-link text-white btn btn-primary" href="tel:' . do_shortcode("[lg-phone-alt]") . '">'. do_shortcode("[lg-phone-main]") .'</a></li>';
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

add_action('wp_content_top', 'featured_banner_top', 1); // ('wp_content_top', defined function name, order)

    function featured_banner_top(){
        ob_start();
        get_template_part('/templates/template-parts/page/top-banner');
        echo ob_get_clean();
    }

/*********************** AJAX example ***************************/
function example_ajax_request() {
 
    if ( isset($_REQUEST) ) {
        $fruit = $_REQUEST['fruit'];

        if ( $fruit == 'Banana' ) {
            $fruit = do_shortcode('[custom-facebook-feed]');
        }
        echo $fruit;
    }
   die();
}
 
add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );

add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );

function example_ajax_enqueue() {
    wp_localize_script(
        'lg-script-child',
        'example_ajax_obj',
        array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )
    );
}
add_action( 'wp_enqueue_scripts', 'example_ajax_enqueue' );

/*********************** AJAX example end ***************************/

/** custom excerpt changes **/
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}
 
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  } 
  $content = preg_replace('/[.+]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]>', $content);
  return $content;
}

// remove dashicons in frontend to non-admin
function lg_dequeue_dashicon() {
  if (current_user_can( 'update_core' )) {
    return;
  }
  wp_deregister_style('dashicons');
}
add_action( 'wp_enqueue_scripts', 'lg_dequeue_dashicon' );

