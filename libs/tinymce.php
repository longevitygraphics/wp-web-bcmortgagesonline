<?php

	global $lg_tinymce_custom;
	
	$lg_tinymce_custom = array(
	    'title' => 'Custom',
	    'items' =>  array(
	    	array(
				'title' => 'Title Underline',
	            'selector' => 'h1, h2, h3, h4, h5, h6',
	            'classes' => 'title-underline'
			),
			array(
				'title' => 'Title Underline Center',
	            'selector' => 'h1, h2, h3, h4, h5, h6',
	            'classes' => 'title-underline-center'
			),
			array(
				'title' => 'List Padding',
	            'selector' => 'ul',
	            'classes' => 'list-padding'
			),
			array(
				'title' => 'Tick',
	            'selector' => 'li',
	            'classes' => 'tick'
			),
			array(
				'title' => 'Body Text Link',
	            'selector' => 'a',
	            'classes' => 'link-bodytext'
			)
	    )
	);

?>