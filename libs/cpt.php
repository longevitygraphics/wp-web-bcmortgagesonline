<?php

    function lg_custom_post_type(){
      register_post_type( 'service',
          array(
            'labels' => array(
              'name' => __( 'Services' ),
              'singular_name' => __( 'Service' )
            ),
            'public' => true,
            'rewrite' => array(
              'with_front' => false
            ),
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );
      register_post_type( 'team-member',
          array(
            'labels' => array(
              'name' => __( 'Team Members' ),
              'singular_name' => __( 'Team Member' )
            ),
            'public' => true,
            'rewrite' => array(
              'with_front' => false
            ),
            'has_archive' => false,
            'menu_icon'   => 'dashicons-admin-users',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );
    }

    add_action( 'init', 'lg_custom_post_type' );

    function lg_custom_taxonomy(){

        register_taxonomy(
          'service-category',
          'service',
          array(
            'label' => __( 'Category' ),
            'rewrite' => array( 
              'slug' => 'service-category',
              'with_front' => false
            ),
            'hierarchical' => true,
          )
        );

        add_submenu_page(
           'lg_menu',
           __('Service Category'),
           __('Service Category'),
           'edit_themes',
           'edit-tags.php?taxonomy=service-category'
        );

        register_taxonomy(
          'team-member-category',
          'team-member',
          array(
            'label' => __( 'Category' ),
            'rewrite' => array( 
              'slug' => 'team-member-category',
              'with_front' => false
            ),
            'hierarchical' => true,
          )
        );

        add_submenu_page(
           'lg_menu',
           __('Team Member Category'),
           __('Team Member Category'),
           'edit_themes',
           'edit-tags.php?taxonomy=team-member-category'
        );
    }

    add_action( 'init', 'lg_custom_taxonomy' );


?>