// Window Scroll Handler

(function($) {
	function isScrolledIntoView(elem) {
	    var docViewTop = $(window).scrollTop();
	    var docViewBottom = docViewTop + $(window).height();

	    var elemTop = $(elem).offset().top;
	    var elemBottom = elemTop + $(elem).height();

	    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	  }
    $(window).on('scroll', function(){
	   	$('.scroll-fadeInLeft').each(function() {
	      if (isScrolledIntoView(this) === true) {
	      	console.log($(this).attr('class'));
	        $(this).addClass('animated fadeInLeft');
	        if($(this).has('[class^="scroll"]')){
	        	console.log($(document).find('[class^="scroll"]').length);
	        }
	      }
	    });
    })

}(jQuery));