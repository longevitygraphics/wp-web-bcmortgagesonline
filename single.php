<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

<?php

	$banner_height = get_option('lg_option_blog_single_banner_height') ? get_option('lg_option_blog_single_banner_height') : '400px';

?>
	<main>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="container pb-5">
				<div class="overlay text-center">
					<h1><?php the_title(); ?></h1>
					<p class="text-uppercase h5 my-3"><i class="far fa-clock mr-1"></i><?php the_date(); ?>
					</p>
				</div>
				<?php the_content(); ?>
			</div>

		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>