<?php

	switch ( get_row_layout()) {
		case 'slick_thumbnail':
			get_template_part('/layouts/layouts/slick-thumbnail-content');
		break;
		case 'card_overlay':
			get_template_part('/layouts/layouts/card-overlay');
		break;
		case 'two_column_procedure':
			get_template_part('/layouts/layouts/two-column-procedure');
		break;
		case 'all_services':
			get_template_part('/layouts/layouts/all-services');
		break;
		case 'gallery_layout':
			get_template_part('/layouts/layouts/gallery-layout');
		break;
		case 'testimonials_videos':
			get_template_part('/layouts/layouts/testimonial-videos');
		break;
		case 'include_community_partners':
			get_template_part('/layouts/layouts/include-community-partners');
		break;
		case 'team_gallery':
			get_template_part('/layouts/layouts/team-gallery');
		break;
		case 'mortgage_calculator':
			get_template_part('/layouts/layouts/mortgage-calculator');
		break;
	}

?>