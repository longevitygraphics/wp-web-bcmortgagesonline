<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			   <?php if (have_rows("services_content", "options")): ?>
			   		<div class="all-services d-flex justify-content-center flex-wrap">
						<?php while(have_rows("services_content", "options")): the_row(); ?>
						<div class="service-card-wrapper">
							<div class="card px-2 py-3">
						<?php  
							$icon = get_sub_field("icon", "options");
							$content = get_sub_field("content", "options");
						?>
						<?php if ($icon): ?>
							<div class="service-icon">
								<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
							</div>
						<?php endif ?>
						<?php if ($icon): ?>
							<div class="service-short-content">
								<?php echo $content; ?>
							</div>
						<?php endif ?>
							</div>
						</div>
						<?php endwhile ?>
					</div>
			   <?php endif ?>
			   	<?php 
				$services_cta = get_field("services_cta", "options");
				if($services_cta) :
					$link_url = $services_cta['url'];
					$link_title = $services_cta['title'];
					$link_target = $services_cta['target'] ? $services_cta['target'] : '_self';
				?>
				<div class="pt-4 m-auto d-flex justify-content-center">
					<a class="btn btn-primary" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
				</div>
				<?php endif ?>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>