
<?php
$args = array(
    'posts_per_page' => 3,
    'post_type' => 'post'
);

$result = new WP_Query($args);

// Loop
if ($result->have_posts()): ?>
        <div class="recent-news row">
        <?php while ($result->have_posts()):

         $result->the_post();
         $title = get_the_title();
         $content = get_the_content();
         $link = get_field('link');
         $image = get_the_post_thumbnail_url();
         $limit = 30;
         ?>
        
        <div class="mb-3 mb-md-0 col-md-4 recent-news-content d-flex flex-direction-column justify-content-between">

            <?php if ($content): ?>
                <div>
                    <h3 class="h5 mb-3"><?php echo $title; ?></h3>
                    <div class="mb-3"><?php echo excerpt($limit); ?></div>
                    <a class="text-secondary" href="<?php echo get_the_permalink(); ?>" class="read-more">Read More</a>
                </div>
            <?php endif; ?>
        </div>
        <?php
     endwhile; ?>
        </div>
        <div class="d-flex justify-content-center mt-4"><a class="btn btn-primary btn-sm" href="/blog">View All News</a></div>
    <?php endif; // End Loop

wp_reset_query();

?>
