<?php if (!is_home()): ?>
<div class="top-banner <?php if(is_front_page()){echo 'home-top-banner';} ?>">
	<?php 
		$top_banner_images = get_field("top_banner")['gallery'];
		$gallery = 	$top_banner_images;
		$text_overlay = get_field("text_overlay");
		$top_banner_form = get_field("top_banner_form"); 
	?>
	<?php if($top_banner_images) : ?>
		<?php include(locate_template('/layouts/components/gallery.php')); ?>
			<?php if($text_overlay || $top_banner_form) : ?>
				<div class="top-banner-overlay <?php if(is_front_page()){echo 'home-top-banner-overlay';} ?>">
					<div class="banner-text container d-xl-flex justify-content-between align-items-center">
						<div>
							<?php echo $text_overlay ?>
						</div>
						<?php if ($top_banner_form): ?>
							<div class="top-banner-form d-none d-xl-inline-block">
								<?php echo do_shortcode($top_banner_form); ?>
							</div>
							<div class="d-xl-none">
								<a class="btn btn-primary btn-sm" href="/contact">Book Appointment</a>
							</div>
						<?php endif ?>
					</div>
				</div>
		<?php endif; ?>
	<?php endif ?>
</div>
<?php endif ?>